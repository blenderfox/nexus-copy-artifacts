## Nexus Artifact Copy

A simple hacky script to copy artifacts from one Nexus repo to another Nexus repo.

Run as follows:

```
SOURCE_NEXUS="url-of-source-nexus" \
SOURCE_CREDENTIALS="user:pass" \
DEST_NEXUS="url-of-dest-nexus" \
DEST_CREDENTIALS="user:pass" \
REPO="maven-snapshots maven-releases" \
make copy-components
```

This will copy artifacts from the maven-snapshots and maven-releases artifacts, download them one at a time, then upload them to the destination nexus.

The script uses `SOURCE_CREDENTIALS` to talk to the source nexus (`SOURCE_NEXUS`) and similarly, uses `DEST_CREDENTIALS` to talk to the destination nexus (`DEST_NEXUS`)

This script can run for a long time depending on the artfacts to copy. It does not do any filtering, and will copy everything it can find. This script is primarily meant for full-mirroring.

The destination repo should be set to allow redeploy, otherwise the attempt to upload artifacts that are already uploaded will fail

The script will download files into `download-tmp` as it goes -- it attempts to keep things minimal by deleting download files when they're finished with.

Mac users may need to use `gmake` instead of `make`

There is also a dockerfile you can use to run this script. It can also be used to run this inside Docker-based cluster (ECS, Kubernetes, etc.)

You can use the kubernetes job sample to run this script as a kubernetes job, or adapt it for use as a cronjob for a regular mirroring to a secondary Nexus
