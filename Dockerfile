FROM debian

RUN apt-get update && \
    apt-get install -y curl jq make && \
    rm -rf /var/lib/apt/lists/*

COPY makefile .

ENV SOURCE_NEXUS={fill-in-details-here}
ENV SOURCE_CREDENTIALS={fill-in-details-here}
ENV DEST_NEXUS={fill-in-details-here}
ENV DEST_CREDENTIALS={fill-in-details-here}
ENV REPO="{fill-in-details-here}"

RUN mkdir download-tmp

CMD [ "make","copy-components" ]
