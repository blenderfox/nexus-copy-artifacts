.ONESHELL:
MAKEFLAGS = -s
# .SHELLFLAGS = -ec
SHELL = bash
#REPO=maven-snapshots maven-releases
STOP_ON_UPLOAD_FAIL = 1 #0=stop on fail, anything else, continue on

export NOTIFIER ## Used for slack notifications, does nothing otherwise
export STOP_ON_UPLOAD_FAIL


clean:
	rm -rfv download-tmp/*

validate-args:
	if [ -z $$SOURCE_NEXUS ] || \
		 [ -z $$DEST_NEXUS ]; then
		 	echo Please provide values for SOURCE_NEXUS and DEST_NEXUS
			exit 1
	fi

slack-notify-start:
	NOTIFIER=$$NOTIFIER ./slackNotifyStart.sh

slack-notify-end-success:
	NOTIFIER=$$NOTIFIER ./slackNotifyEndSuccess.sh

copy-components:
	if [ -z "$$SOURCE_NEXUS" ] || \
		 [ -z "$$SOURCE_CREDENTIALS" ] || \
		 [ -z "$$DEST_NEXUS" ] || \
		 [ -z "$$DEST_CREDENTIALS" ] || \
		 [ -z "$$REPO" ]; then
			echo Please provide values for SOURCE_NEXUS, SOURCE_CREDENTIALS, DEST_NEXUS, DEST_CREDENTIALS and REPO
			exit 1
	fi
	echo Copying from repo list: $$REPO
	for repo in $$REPO
	do
		echo Downloading components list at $$SOURCE_NEXUS/service/rest/v1/components?repository=$$repo using credentials $$SOURCE_CREDENTIALS

		if [ ! -d download-tmp ]; then mkdir -v download-tmp;fi

		curl -su $$SOURCE_CREDENTIALS --retry 60 --retry-delay 30 --fail "http://$$SOURCE_NEXUS/service/rest/v1/components?repository=$$repo" > download-tmp/download.json
		if [ $$? -ne 0 ]; then
			echo "Can't get component list, check output below"
			curl -svu $$SOURCE_CREDENTIALS --retry 60 --retry-delay 30 --fail "http://$$SOURCE_NEXUS/service/rest/v1/components?repository=$$repo" -o /dev/null
			exit 1
		fi

		token=$$(cat download-tmp/download.json | jq --raw-output '.continuationToken')

		while [ true ]
		do
		echo Loop processing starting, with next continuation token $$token
		OLDIFS=$$IFS
		IFS=$$'\n'

		cat download-tmp/download.json | jq .items[] -c | \
		while read i
		do
			echo $$i >>download-tmp/download-items.json
		done

		IFS=$$OLDIFS

		for line in $$(cat download-tmp/download-items.json)
		do
			items+=($$line)
		done

		# for item in $${items[@]}
		# do
		# 	echo $$item
		# done

		for item in $${items[@]}
		do
			# echo $$item

			downloadurl=$$(echo $$item | jq --raw-output '.assets[].downloadUrl')
			groupid=$$(echo $$item | jq --raw-output '.group')
			artifactid=$$(echo $$item | jq --raw-output '.name')
			version=$$(echo $$item | jq --raw-output '.version')
			if [ "$$repo" == "maven-snapshots" ]; then
				version="$$version-SNAPSHOT"
			fi

			## Check ext against list
			for url in $$downloadurl
			do
				echo "Downloading $$url"
				curl -su $$SOURCE_CREDENTIALS $$url >download-tmp/$$(basename $$url)
				echo "Uploading $$(echo $$url | sed s/"$$SOURCE_NEXUS\/"/"$$DEST_NEXUS\/"/g)"
				curl -s --fail -u $$DEST_CREDENTIALS \
					--upload-file download-tmp/$$(basename $$url) \
					"$$(echo $$url | sed s/"$$SOURCE_NEXUS"/"$$DEST_NEXUS"/g)" 2>&1
					if [ $$? -ne 0 ]; then
						echo Failed to upload $$(echo $$url | sed s/"$$SOURCE_NEXUS"/"$$DEST_NEXUS"/g)
						curl -vs --fail -u $$DEST_CREDENTIALS \
							--upload-file download-tmp/$$(basename $$url) \
							"$$(echo $$url | sed s/"$$SOURCE_NEXUS"/"$$DEST_NEXUS"/g)" 2>&1

						## Break on fail if flag is set
						if [ $$STOP_ON_UPLOAD_FAIL -eq 0 ]; then
							## Stop
							echo Sending slack notification and then bailing
							NOTIFIER=$$NOTIFIER \
							MSG="Nexus Mirror Process Did Not Finish Successfully" \
							./slackNotifyEndFail.sh
							exit 1
						else
							## Continue on
							echo Continuing on as STOP_ON_UPLOAD_FAIL is not set to zero...
						fi



					else
						echo Copy OK
					fi
				rm -v download-tmp/$$(basename $$url)
				echo ""
			done

		done

		rm download-tmp/download.json download-tmp/download-items.json
		if [ ! "$$token" == "null" ]; then
			echo Fetching next page, continuation token is $$token
			curl -su $$SOURCE_CREDENTIALS --fail "http://$$SOURCE_NEXUS/service/rest/v1/components?repository=$$repo&continuationToken=$$token" > download-tmp/download.json || \
				( echo Failed to get components list; exit 1)
		else
			echo Continuation token is nil, terminating
			exit 0
		fi
		token=$$(cat download-tmp/download.json | jq --raw-output '.continuationToken')
		echo New continuation token is $$token
		done

	done
